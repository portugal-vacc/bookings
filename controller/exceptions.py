class ExternalServiceClientErrorResponse(Exception):
    pass


class ExternalServiceServerErrorResponse(Exception):
    pass


class ExternalServiceTypeVerificationError(Exception):
    pass


class DualBookingError(Exception):
    pass


class MinimumBookingTimeError(Exception):
    pass
