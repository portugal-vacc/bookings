from django.contrib import admin

from atc.models.booking import Booking

admin.site.register(Booking)
