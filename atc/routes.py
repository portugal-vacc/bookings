from typing import List

from ninja import Router

from atc.bridge import future_bookings, new_booking, this_booking
from atc.schemas.booking import BookingIn, BookingOut
from atc.services import create_vatsim_booking

atc_router = Router(tags=["atc"])
app_name = "atc"


@atc_router.get("/", response=List[BookingOut])
def get_bookings(request, client_id: int = None):
    return future_bookings(client_id)


@atc_router.post("/")
def post_booking(request, payload: BookingIn):
    booking = new_booking(payload)
    create_vatsim_booking(booking)
    return {"success": True} if booking else {"success": False}


@atc_router.get("/{booking_id}", response=BookingOut)
def get_booking(request, booking_id: int):
    return this_booking(booking_id)


@atc_router.delete("/{booking_id}")
def delete_booking(request, booking_id: int):
    booking = this_booking(booking_id)
    action = booking.delete()
    return {"success": True} if action[0] == 1 else {"success": False}
