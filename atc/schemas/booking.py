from datetime import datetime, time, date, timezone, timedelta
from enum import Enum
from typing import Optional

from ninja import Schema, ModelSchema
from pydantic import validator, root_validator

from atc.models.booking import Booking
from atc.schemas.callsigns import CallsignsChoices
from controller.exceptions import MinimumBookingTimeError


class BookingTypeChoices(str, Enum):
    booking = 'booking'
    event = 'event'
    exam = 'exam'
    mentoring = 'mentoring'


class BookingModel(Schema):
    type: BookingTypeChoices
    client_id: int
    callsign: CallsignsChoices
    start_date: datetime
    end_date: datetime
    mentor: Optional[str]

    @root_validator(pre=True)
    def end_time_different_than_start_time(cls, values):
        start_date, end_date = values.get('start_date'), values.get('end_date')
        delta = end_date - start_date
        if delta < timedelta(minutes=30):
            raise MinimumBookingTimeError()
        return values


class BookingIn(Schema):
    type: BookingTypeChoices
    client_id: int
    callsign: CallsignsChoices
    start_date: date
    start_time: time
    end_time: time
    mentor: Optional[int] = None

    @root_validator(pre=True)
    def end_time_different_than_start_time(cls, values):
        start_time, end_time = values.get('start_time'), values.get('end_time')
        if start_time == end_time:
            raise ValueError('Start and End time are the same...')
        return values

    @root_validator(pre=True)
    def mentor_type_force_mentor(cls, values):
        booking_type, mentor = values.get('type'), values.get('mentor')
        if booking_type == BookingTypeChoices.mentoring and not mentor:
            raise ValueError("It's a mentoring booking without a mentor?")
        elif mentor and not booking_type == BookingTypeChoices.mentoring:
            raise ValueError("It's booking has mentor but it's not mentoring type?")
        return values

    @validator('start_date')
    def start_date_today_or_after(cls, v):
        assert v >= date.today(), 'Start day must be today or after'
        return v

    @property
    def booking_start_date(self) -> datetime:
        return datetime.combine(date=self.start_date, time=self.start_time, tzinfo=timezone.utc)

    @property
    def booking_end_date(self) -> datetime:
        _end_date = datetime.combine(date=self.start_date, time=self.end_time, tzinfo=timezone.utc)
        if self.start_time > self.end_time:
            _end_date = _end_date.replace(day=self.start_date.day + 1)
        return _end_date

    @property
    def booking_mentor(self) -> Optional[int]:
        return self.mentor if self.mentor else None

    def convert_to_model(self) -> BookingModel:
        return BookingModel(type=self.type, client_id=self.client_id, callsign=self.callsign,
                            start_date=self.booking_start_date, end_date=self.booking_end_date,
                            mentor=self.booking_mentor)


class BookingOut(ModelSchema):
    class Config:
        model = Booking
        model_fields = "__all__"
