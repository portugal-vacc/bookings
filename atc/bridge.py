from datetime import datetime, timezone
from typing import List

from django.db.models import QuerySet
from django.shortcuts import get_object_or_404

from atc.models.booking import Booking
from atc.schemas.booking import BookingModel, BookingIn, BookingOut
from controller.exceptions import DualBookingError


def check_already_exists(candidate: BookingModel):
    bookings = Booking.objects.all()
    for booking in bookings:
        if candidate.callsign == booking.callsign \
                and candidate.start_date >= booking.start_date \
                and candidate.end_date <= booking.end_date:
            raise DualBookingError(str(booking))


def future_bookings(client_id: int) -> List[BookingOut]:
    if client_id:
        by_client = Booking.objects.filter(client_id=client_id, end_date__gte=datetime.now(tz=timezone.utc)).order_by(
            'start_date')
        by_mentor = Booking.objects.filter(mentor=client_id, end_date__gte=datetime.now(tz=timezone.utc)).order_by(
            'start_date')
        return list(by_client) + list(by_mentor)
    return Booking.objects.filter(end_date__gte=datetime.now(tz=timezone.utc)).order_by('start_date')


def new_booking(payload: BookingIn) -> BookingModel:
    booking: BookingModel = payload.convert_to_model()
    check_already_exists(booking)
    Booking(**booking.dict()).save()
    return booking


def this_booking(booking_id: int) -> Booking:
    return get_object_or_404(Booking, id=booking_id)


def client_bookings(client_id: int) -> QuerySet:
    return Booking.objects.filter(client_id=client_id).order_by('start_date')


def mentor_bookings(mentor_id: int) -> QuerySet:
    return Booking.objects.filter(mentor=mentor_id).order_by('start_date')
