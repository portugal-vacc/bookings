from datetime import datetime

from django.db import models


class Booking(models.Model):
    type: str = models.CharField(max_length=20)
    client_id: int = models.IntegerField(null=False)
    callsign: str = models.CharField(max_length=30)
    start_date: datetime = models.DateTimeField()
    end_date: datetime = models.DateTimeField()
    mentor: int = models.IntegerField(null=True, blank=True)
    created_at: datetime = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"[{self.callsign}] {self.type} - {self.client_id} {f'| Mentor {self.mentor}' if self.mentor else ''}"
